///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab @Todo
//
//
// Usage: @Todo
//
//
// @author Vinton Sistoza <sistozav@wiliki>
// @date   @Todo
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <array>
#include <list>
#include <ctime>
#include "animal.hpp"
#include "animalfactory.hpp"

using namespace std;
using namespace animalfarm;
int Animal::i;
int Animal::length;
char Animal::randomName[10];
int main(){
   srand(time(0));

      //Build Array
      array<Animal*, 30> animalArray;
      animalArray.fill(NULL);
      cout << "Welcome to Animal Farm 3" << endl;
   
   
      //Create Animals for Array
      for(int i=0 ; i<25 ; i++)
         animalArray[i] = AnimalFactory::getRandomAnimal();
      cout << endl;

      //Create Output
      cout << "Array of Animals" << endl;
      cout << "   Is it empty:   "; 
      if(animalArray.empty())
         cout << "True" << endl;
      else
         cout << "False" << endl;
      cout << "   Number of elements:   " << animalArray.size() << endl;
      cout << "   Max size:   " << animalArray.max_size() << endl;
      //Animal Speak
      for(int i = 0 ; i <25 ; i++)
            cout << animalArray[i]->speak() << endl;
      for(Animal* animal:animalArray)
         delete animal;

      //BUILD LIST
      list<Animal*> animalList;
      //Create Animals for List
      for(int i=0 ; i<25 ; i++)
         animalList.push_front(AnimalFactory::getRandomAnimal());
      cout << endl;
      
      //Output
      cout << "Array of Animals" << endl;
      cout << "   Is it empty:   "; 
      if(animalList.empty())
         cout << "True" << endl;
      else
         cout << "False" << endl;
      cout << "   Number of elements:   " << animalList.size() << endl;
      cout << "   Max size:   " << animalList.max_size() << endl;
      //Animal Speak
      for(Animal* animal:animalList)
         cout << animal->speak() << endl;
      for(Animal* animal:animalList)
         delete animal;
      cout << endl;
      
   return 0;
}
